package dev.amrro.andela.ui

import android.net.http.SslError
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.SslErrorHandler
import android.webkit.WebResourceRequest
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.navigation.ui.setupWithNavController
import dev.amrro.andela.R
import dev.amrro.andela.databinding.WebViewFragmentBinding


class WebViewFragment : Fragment() {

    private lateinit var binding: WebViewFragmentBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.web_view_fragment, container, false)

        // Setting up the toolbar.
        with(activity as AppCompatActivity) {
            this.setSupportActionBar(binding.toolbar)
            binding.toolbar.setupWithNavController(this@WebViewFragment.findNavController())
            this.supportActionBar?.title = "About"
        }

        // Setting up the WebView.
        binding.webview.settings.javaScriptEnabled = true
        binding.webview.webViewClient = object : WebViewClient() {

            override fun onReceivedSslError(
                view: WebView?,
                handler: SslErrorHandler?,
                error: SslError?
            ) {
                handler?.proceed()
            }

            override fun shouldOverrideUrlLoading(
                view: WebView?,
                request: WebResourceRequest?
            ): Boolean {
                view?.loadUrl(request?.url.toString())
                return false
            }
        }
        binding.webview.loadUrl(getString(R.string.andela_url))
        return binding.root
    }

}
