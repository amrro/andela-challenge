package dev.amrro.andela.ui

import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.browser.customtabs.CustomTabsIntent
import androidx.core.net.toUri
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import dev.amrro.andela.R
import dev.amrro.andela.databinding.HomeFragmentBinding
import dev.amrro.andela.util.chrome.CustomTabActivityHelper


class HomeFragment : Fragment() {

    private lateinit var binding: HomeFragmentBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(
            inflater,
            dev.amrro.andela.R.layout.home_fragment,
            container,
            false
        )

        // Setting up the toolbar.
        with(activity as AppCompatActivity) {
            this.setSupportActionBar(binding.toolbar)
        }

        // Handle different user's clicks.
        binding.handler = object : HomeFragmentHandler {
            override fun toWebViewFragment() {
//                this@HomeFragment.findNavController()
//                    .navigate(HomeFragmentDirections.toWebviewFragment())

                launchCustomTab()

            }

            override fun toProfileFragment() {
                this@HomeFragment.findNavController()
                    .navigate(HomeFragmentDirections.toProfileFragment())
            }
        }
        return binding.root
    }


    interface HomeFragmentHandler {
        fun toWebViewFragment()
        fun toProfileFragment()
    }


    /**
     * This method launch the Chrome Custom Tab.
     * If Custom Tabs is not available, It will use the [WebViewFragment]
     */
    private fun launchCustomTab() {
        val customTab = CustomTabsIntent.Builder().apply {
            setStartAnimations(requireContext(), R.anim.slide_in_right, R.anim.slide_out_left)
            setExitAnimations(
                requireContext(),
                android.R.anim.slide_in_left,
                android.R.anim.slide_out_right
            )

            setShowTitle(true)
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
                setToolbarColor(requireContext().getColor(R.color.colorPrimary))
            else
                setToolbarColor(resources.getColor(R.color.colorPrimary))

        }.build()

        CustomTabActivityHelper.openCustomTab(
            activity,
            customTab,
            getString(R.string.andela_url).toUri()
        ) { _, _ ->
            this.findNavController()
                .navigate(HomeFragmentDirections.toWebviewFragment())
        }
        customTab.launchUrl(context, Uri.parse(getString(R.string.andela_url)))
    }


}
