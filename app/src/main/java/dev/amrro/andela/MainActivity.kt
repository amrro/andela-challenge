package dev.amrro.andela

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity


/**
 * This Activity is the base activity for all application.
 * Using Single-Activity-Multiple-Fragment style.
 */
class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
}
